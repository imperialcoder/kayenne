<h1 align="center">Kayenne</h1>

<h4 align="center">An Ansible Playbook for Managing Gentoo Servers + Workstations + K3S Custer</h4>

## Key Features

* Full Ansible Config - the ability to create/manage linux servers and the K3S Custers
* Virtual Development Env - using vagrant, has the ability to run on any machine!
* a Packer Image build (WIP) - Ability to create cloud images to deploy everywhere!
* Roles, Roles, and More Roles - lots of roles for all of my linux/windows/server needs
* A Justfile - making some of the more repetitive/tedious commands a little easier

## First time? 🪢

**BEFORE BEGINNING** this guide assumes you have a basic knowledge of ssh with how to connect to servers and ansible with how 
to run playbooks and roles. To clone and run this application, you'll need 
[Git](https://git-scm.com), 
[Ansible](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html), 
[OpenSSH](https://www.openssh.com/), 
and [Vagrant](https://www.vagrantup.com/docs/installation) 
installed on your computer.

There are a few ways to use this repo, each with there own dedicated sections:

 * [The Justfile](#the-justfile):

    Some command that'd be useful in general while using this ansible-playbook

 * [Ansible Pull](#ansible-pull) (💻 <= ☁️)):

    The Git Repo Controls your Computer(s)/Server(s) via Ansible Pull 
    
 * [Classic Ansible](#classic-ansible) (💻 => ☁️️️☁️️️☁️️️)): 

    Your Computer Controls your Server(s) via Ansible
    
 * [Vagrant](#vagrant) (💻 => 💽 💽 💽):

    Your Computer Controls Vagrant VM's via Ansible

 * [License](#license)

    need to protect my work some how ¯\_(ツ)_/¯
   
That's it! you're already an ansible pro (well not quiet yet). I hope this helps on your automation journey.

## The Justfile

I've gone ahead and created a [justfile](https://github.com/casey/just) to make some commands easier to run.
Once you've installled the just runner, you should be able to list all runners using the command:
```bash
$ just
```
these are constantly changing, so I won't provide too much detail

## Ansible Pull

this one is the simplest of the three, ansible pull does what it says, right in the name ;) we just pull from out git repo and WAM it's installed! **DO NOTE** you need to have ansible installed (obvi) but you you don't need anything else!

``` bash
$ ansible-pull -o -U https://gitlab.com/imperialcoder/kayenne.git 
```
 
## Classic Ansible

This is the classic usage for ansible where we have one computer managing a whole fleet of servers. this is great if you want to pick and choose what roles/playbooks to run on each of the servers.

``` bash
# Clone this Repository
$ git clone https://gitlab.com/imperialcoder/kayenne.git && cd kayenne

# Create an Ansible SSH key
$ just ssh create

# Create an Ansible Inventory File with all of your servers
$ echo -e "192.168.1.1\n google.com" >> inventory/remote

# Bootstrap newly added servers
$ just play bootstrap -k

# Lets test is out!
$ ansible all -m ping
```

## Vagrant

This is the playground environment (just in case you don't have a few servers or want to try something out) totally free to trash and gunk up with whatever your heart desires :) it's a great starting point to learn and try out ansible.

``` bash
# Clone this Repository
$ git clone https://gitlab.com/imperialcoder/kayenne.git && cd kayenne

# Create an Ansible SSH key
$ just ssh create

# Setup Vagrant
$ just vagrant reset

# build + run vagrant
$ just vagrant up

# Test it out!
$ ansible all -m ping
```

## License

[GPLV3](LICENSE)

---

> [jesseg.co](https://www.jesseg.co) &nbsp;&middot;&nbsp;
> GitLab [@imperialcoder](https://gitlab.com/imperialcoder) &nbsp;&middot;&nbsp;
<!-- > Twitter [@amit_merchant](https://twitter.com/amit_merchant) -->

