# general variables
variable "ssh_username" {
    type = string
    default = "root"
}

variable "ssh_password" {
    type = string
    default = "password"
    sensitive = true
}

variable "ssh_private_key_file" {
  type    = string
  default = "~/.ssh/ansible"
}

variable "efi_firmware" {
    type = map(string) 
    default = {
        code = "/usr/share/edk2-ovmf/OVMF_CODE.fd"
        vars = "/usr/share/edk2-ovmf/OVMF_VARS.fd"
    }
}

variable "gentoo_iso" {
    type = map(string)
    default = {
        url = "https://distfiles.gentoo.org/releases/amd64/autobuilds/20250216T164837Z/install-amd64-minimal-20250216T164837Z.iso"
        checksum = "sha256:c7da771b38b7d564caadf6a3bc4334a19b2b9a3c95c46d3b7ab15fcac18e6e7f"
    }
}
