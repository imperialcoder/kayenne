# debugging
set -x
echo "********** starting **********"

# load variables we defined
source "/etc/libvirt/hooks/kvm.conf"

# stop display manager
rc-service display-manager stop

# Unbind VTconsoles
echo 0 > /sys/class/vtconsole/vtcon0/bind
echo 0 > /sys/class/vtconsole/vtcon1/bind

# Unbind EFI-framebuffer
echo efi-framebuffer.0 > /sys/bus/platform/drivers/efi-framebuffer/unbind

# avoid race conditions
sleep 2 

# unload Nvidia
modprobe -r nvidia
modprobe -r nvidia_modeset
modprobe -r nvidia_uvm
modprobe -r nvidia_drm

# unbind gpu
virsh nodedev-detach $VIRSH_GPU_VIDEO
virsh nodedev-detach $VIRSH_GPU_AUDIO

# load vfio
modprobe vfio
modprobe vfio_pci
modprobe vfio_iommu_type1
