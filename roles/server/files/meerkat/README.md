# Meerkat

A helmchart of services that manages certicates, web traffic, and projects.

this project uses the following open-source packages: 
- [Cert-Manager](https://cert-manager.io/)
- [Traefik](https://traefik.io/)
- [Gitea](https://gitea.io/en-us/)
- [Drone](https://www.drone.io/)

# Key Features

- freedom to use with whatever cloud-provider/server you wish.
- automatic management of certicates, ingress-controller, and deployments.
- a completly self-hosted alternative to something like Github and AWS.

# Motivation

I got too sick of AWS and Github/Gitlab dependent services that constantly 
change pricing and feature availability. this is just me revolting against
that. Also needed these to orcistrate easily together with minimal effort.
I hope to someday use this for future clients and will use this in my 
personal server stack.
