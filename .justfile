PWD := "$(shell pwd)"

# list all just commands
default:
    @just --list --unsorted

# runs everything that's needed to get started
init: (submodule 'init') (ssh 'create') (ansible 'play' 'bootstrap')

# manages the submodules in ansible roles
submodule $command *$options='':
    #!/usr/bin/env bash
    if [[ $command == "init" ]]; then
        git submodule update --recursive --init $options
    elif [[ $command == "update" ]]; then
        git submodule update --recursive --remote $options
    fi

# creates/copies ssh keys to hosts
ssh $command *$options='':
    #!/usr/bin/env bash
    if [[ $command == "create" ]]; then
        ssh-keygen $options -t ed25519 -f ~/.ssh/ansible
    elif [[ $command == "copy" ]]; then
        read -p "username: " USER
        read -p "ssh server: " SSH
        ssh-copy-id $options -i ~/.ssh/ansible.pub $USER@$SSH
    fi


VAGRANT_ANSIBLE_INV := PWD + "/.vagrant/provisioners/ansible/inventory/vagrant_ansible_inventory"
# ------------------------------------------------------------------
# allows you to run vagrant commands in a more intuitive way for project.
# examples:
#   ## will reset local vagrant files for a fresh environment
#   - just vagrant reset
#
#   ## aliases any vagrant command you can think of
#   - just vagrant up | just vagrant destroy | etc...
# ------------------------------------------------------------------
#
# vagrant (reset|up|<vagrant --help>) $options
vagrant $command *$options='':
    #!/usr/bin/env bash
    if [[ $command == "reset" ]]; then
        [[ -d ./.vagrant/ ]] && rm -rf .vagrant
        vagrant plugin update $options
    elif [[ $command == "up" ]]; then
        vagrant up $options
        ln -sf $VAGRANT_ANSIBLE_INV inventory/vagrant
    else
        vagrant $command $options
    fi

# ------------------------------------------------------------------
# allows you to run packer commands in a more intuitive way for the project.
# examples:
#   ## will reset local packer files for a fresh environment
#   - just packer reset
#
#   ## will allow for some advanced debugging on build
#   - just packer debug
#
#   ## aliases any packer command you can think of
#   - just packer build | just packer console | etc...
# ------------------------------------------------------------------
#
# packer (reset|debug|build) $target $options
packer $command $target='packer' *$options='':
    #!/usr/bin/env bash
    if [[ $command == "reset" ]]; then
        [[ -d ./packer/output/ ]] && rm -rf packer/output
        packer init $options $target
    elif [[ $command == "debug" ]]; then
        PACKER_LOG=1 just packer build $target -on-error=ask
    elif [[ $command == "build" ]]; then
        packer build -force -on-error=ask $options $target
    else
        packer $command $options $target
    fi


# ------------------------------------------------------------------
# just a useful script to make dealing with and modifying portage useflags
# a bit easier and have it translate over to the ansible role with ease.
# examples:
#   ## rsync the whole portage directory with the one specified in the role
#   - just portage rsync workstation package.accept_keywords 
#
#   ## diff specified targets to see what has changed
#   - just portage diff workstation package.accept_keywords/chromium
#
#   ## copy specified targets
#   - just portage copy workstation package.accept_keywords/chromium
# ------------------------------------------------------------------
#
# $command=(rsync|diff|) 
# $command=(rysync|diff) - a fuzzy finding utility to rsync / diff usefiles from /etc/portage/$target to ./roles/$role/files/portage/$target
portage $command $role='' $target='' *$options='':
    #!/usr/bin/env bash
    role_dirs=$(find ./roles -type f | grep ".*/tasks/main.yml.*" | awk -F "/tasks/" '{print $1}')
    role_files=$(find ./roles -type f | grep ".*files/portage/package..*")
    port_files=$(find /etc/portage -type f | grep ".*package..*")
    omitted_files=$(printf "$role_files" | grep -v ".*roles/$role.*$target.*")

    function omit_non_matching_files {
        [[ ! -z $@ ]] \
            && cat | grep -E ".*/($(printf "$@" | xargs -n1 basename | tr "\n" "|" | sed 's/|$//')).*" \
            || cat
    }

    function omit_matching_files {
        [[ ! -z $@ ]] \
            && cat | grep -Ev ".*/($(printf "$@" | xargs -n1 basename | tr "\n" "|" | sed 's/|$//')).*" \
            || cat
    }

    if [[ $command == "rsync" ]]; then
        TARGETS=$(printf "$role_files" \
            | omit_non_matching_files "$port_files" \
            | grep ".*$role.*$target.*" \
            | xargs -I {} sh -c 'echo "/etc/portage$(echo "{}" | awk -F "portage" "{print \$2}") {}"')

        printf "$TARGETS" | awk '{gsub(/ /, " -> "); print}'
        printf "rsync the above? " && read -p "(y|n): " ANSWER
        if [[ $ANSWER == "y" ]]; then
            echo "$TARGETS" | while IFS= read -r TARGET; do
                echo "$TARGET" | xargs -n 2 -P 4 sh -c '[[ -e $0 ]] && rsync "$0" "$1" || echo "$0 does not exist!"'
            done
        fi
    elif [[ $command == "diff" ]]; then
        query=$(printf "$port_files" \
            | omit_matching_files "$omitted_files" \
            | grep ".*portage.*$target.*")
        [[ -z $query ]] && echo "no usefiles matching '$target' with matching '$role' role" && exit 0
        homeless=$(printf "$query" | omit_matching_files "$role_files")
        housed=$(printf "$query" | omit_matching_files "$homeless")
        [[ ! -z $homeless ]] && \
            echo -e "\e[31m----- homeless portage files -----\e[0m" \
                    "\n$homeless" \
                    "\n\e[31m----------------------------------\e[0m"
        for SOURCE in $housed; do
            DESTIN=$(printf "$role_files" | grep ".*$(echo $SOURCE | cut -d "/" -f4-).*")
            [[ -f $DESTIN ]] && diff --unified --color $DESTIN $SOURCE
        done
        exit 0
    fi

# completely removes k3s from current system
k3s-reset:
    sudo sh /var/db/repos/gentoo/sys-cluster/k3s/files/k3s-killall.sh
    sudo rm -rf /var/lib/rancher/ /etc/rancher
